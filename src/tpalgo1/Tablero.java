package tpalgo1;

import java.io.Writer;
import java.util.StringTokenizer;
import java.io.IOException;

public class Tablero 
{
	private int ancho;
	private int alto;

	@Override
	public String toString() { return "(Ancho: " + ancho + ", Alto: " + alto + ")"; };
	
	public Tablero(int Ancho, int Alto)
	{
		ancho = Ancho;
		alto = Alto;
	}
	
	public int getAncho() { return ancho; }
	
	public int getAlto() { return alto; }
	
	public static void Escribir(Writer Output, Tablero NTablero) throws IOException
	{
		Output.write("( " + NTablero.getAncho() + " , " + NTablero.getAlto() + " ) ");
	}
	
	public static Tablero Leer(StringTokenizer Input) throws IOException
	{
		int[] dupla = LeerUtil.leerDupla(Input);
		Tablero tablero = new Tablero(dupla[0], dupla[1]);
		
		return tablero;
    }
	
}