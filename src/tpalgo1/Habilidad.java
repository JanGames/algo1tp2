package tpalgo1;

import java.io.Writer;
import java.util.StringTokenizer;
import java.io.IOException;

public class Habilidad 
{
	private TipoHabilidad tipo;
	private int level;
	
	@Override
	public String toString() { return tipo.name() + " " + level; };
	
	public Habilidad(TipoHabilidad Tipo)
	{
		tipo = Tipo;
		level = 1;
	}

	public TipoHabilidad getTipo() { return tipo; }
	
	public int getRango() 
	{
		int rango = 0;
		
		if(tipo == TipoHabilidad.Sanar) { rango = level * 3; }
		else if(tipo == TipoHabilidad.Atacar) { rango = level * 2; }
		else if(tipo == TipoHabilidad.AtacarLejos) { rango = level * 3; }
		else if(tipo == TipoHabilidad.AtacarFuerte) { rango = 1; }
		else if(tipo == TipoHabilidad.ChuckNorris) {
			if(level <= 100) { rango = 1; }
			else { rango = 99999; }
		}
		
		return rango;
	}
	
	public int getPoder() 
	{
		int poder = 0;
		
		if(tipo == TipoHabilidad.Sanar) { poder = level * 5; }
		else if(tipo == TipoHabilidad.Atacar) { poder = level * 3; }
		else if(tipo == TipoHabilidad.AtacarLejos) { poder = level * 2; }
		else if(tipo == TipoHabilidad.AtacarFuerte) { poder = level * 5; }
		else if(tipo == TipoHabilidad.ChuckNorris) {
			if(level <= 100) { poder = 1; }
			else { poder = 99999; }
		}
		
		return poder;
	}

	public int getLevel() { return level; }	
	public void setLevel(int Level) { level = Level; }
    
	public static void Escribir(Writer Output, Habilidad NHabilidad) throws IOException
	{
    	Output.write(NHabilidad.getTipo().name() + " " + NHabilidad.getLevel() + " ");
	}
	
	public static Habilidad Leer(StringTokenizer Input) throws IOException
	{
		Habilidad habilidad = null;
		String token = Input.nextToken();
		
		if(token.equals("Sanar")) { habilidad = new Habilidad(TipoHabilidad.Sanar); }
		else if(token.equals("Atacar")) { habilidad = new Habilidad(TipoHabilidad.Atacar); }
		else if(token.equals("AtacarFuerte")) { habilidad = new Habilidad(TipoHabilidad.AtacarFuerte); }
		else if(token.equals("AtacarLejos")) { habilidad = new Habilidad(TipoHabilidad.AtacarLejos); }
		else if(token.equals("ChuckNorris")) { habilidad = new Habilidad(TipoHabilidad.ChuckNorris); }
		
		habilidad.setLevel(Integer.valueOf(Input.nextToken()));
		
		return habilidad;
    }
	
}