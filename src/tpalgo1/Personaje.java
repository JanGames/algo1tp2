package tpalgo1;

import java.util.ArrayList;
import java.io.Writer;
import java.util.StringTokenizer;
import java.io.IOException;

public class Personaje 
{
	private String nombre;
	private ArrayList<Habilidad> habilidades;
	private int vidaMaxima;
	private int vidaActual;
	private int velocidad;
	private ArrayList<String> victimas;
	
	@Override
	public String toString() { return nombre + " " + vidaActual + "/" + vidaMaxima; };
	
	public Personaje(ArrayList<Habilidad> Habilidades, String Nombre, int Vida, int Velocidad)
	{
		nombre = Nombre;
		habilidades = Habilidades;
		vidaMaxima = Vida;
		vidaActual = Vida;
		velocidad = Velocidad;
		victimas = new ArrayList<String>();
	}
	
	public String getNombre() { return nombre; }
	
	public ArrayList<Habilidad> getHabilidades() { return habilidades; }
	
	public int getVidaMaxima() { return vidaMaxima; }
	
	public int getVidaActual() { return vidaActual; }
	public void	setVidaActual(int Vida) { vidaActual = Vida; }

	public int getVelocidad() { return velocidad; }
	
	public ArrayList<String> getVictimas() { return victimas; }
	public void agregarVictima(String Victima) { victimas.add(Victima);	}
	
	public int getNivelPoder()
	{
		int poder = 0;
		int j = 0;
		while(j < getHabilidades().size()) {
			poder += getHabilidades().get(j).getLevel();
			
			j++;
		}
		
		return poder;
	}	
    
	public static void Escribir(Writer Output, Personaje NPersonaje) throws IOException
	{
    	Output.write(NPersonaje.getNombre() + " ");
    	Output.write(NPersonaje.getVidaMaxima() + " ");
    	Output.write(NPersonaje.getVidaActual() + " ");
    	Output.write(NPersonaje.getVelocidad() + " ");
    	
    	int i = 0;
    	Output.write("[ ");
    	ArrayList<Habilidad> habilidades = NPersonaje.getHabilidades();
    	while(i < habilidades.size()) {
    		Habilidad habilidad = habilidades.get(i);
    		Habilidad.Escribir(Output, habilidad);
    		
    		i++;
    	}
    	Output.write("] ");
    	
    	i = 0;
    	Output.write("[ ");
    	ArrayList<String> victimas = NPersonaje.getVictimas();
    	while(i < victimas.size()) {
    		Output.write(victimas.get(i) + " ");
		    if(i < victimas.size() - 1) { Output.write(", "); }
    		
    		i++;
    	}
    	Output.write("] ");
	}
	
	public static Personaje Leer(StringTokenizer Input) throws IOException
	{
		Personaje personaje = null;
		String token = Input.nextToken();
		
		if(!token.equals("]")) {
			if(token.equals(",")) { token = Input.nextToken(); }
			
			String nombre = token;
			
			int vidaMaxima = Integer.valueOf(Input.nextToken());
			int vidaActual = Integer.valueOf(Input.nextToken());
			int velocidad = Integer.valueOf(Input.nextToken());
			
			ArrayList<Habilidad> habilidades = new ArrayList<Habilidad>();
			token = Input.nextToken();
			if(token.equals("[")) {
				int i = 0;
				
				while(i < 2) {
					habilidades.add(Habilidad.Leer(Input));
					
					i++;
				}
				
				if(!Input.nextToken().equals("]"))
				{ System.err.println("La lista de habilidades termina con ]"); }
			} else { System.err.println("La lista de habilidades empieza con ["); }
			
			ArrayList<String> victimas = new ArrayList<String>();
			token = Input.nextToken();
			if(token.equals("[")) {
				token = Input.nextToken();
				
				while(!token.equals("]")) {
					if(token.equals(",")) { token = Input.nextToken(); }
					
					victimas.add(token);
					
					token = Input.nextToken();
				}
				
				if(!token.equals("]")) {  }
			}
			
			personaje = new Personaje(habilidades, nombre, vidaMaxima, velocidad);
			personaje.setVidaActual(vidaActual);
			
			int i = 0;
			while(i < victimas.size()) {
				personaje.agregarVictima(victimas.get(i));
				
				i++;
			}			
		}
		
		return personaje;
    }
	
}