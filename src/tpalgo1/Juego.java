package tpalgo1;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class Juego 
{
	private Tablero tablero;
	private List<Personaje> personajes;
	private Map<String, Posicion> posiciones;
	
	@Override
	public String toString() {
		return "Tablero: " + tablero + "\n" + "Personajes: " + personajes + "\n" + "Posiciones: " +
				posicionesString(this);
	};
	
	public Juego(List<Personaje> Personajes, Map<String, Posicion> Posiciones, Tablero NTablero)
	{
		tablero = NTablero;
		personajes = Personajes;
		posiciones = Posiciones;
	}
	
	public Tablero getTablero()
	{
		return tablero;
	}

	public ArrayList<Personaje> getPersonajes()
	{
		return (ArrayList<Personaje>) personajes;
	}
	
	public Posicion posicion(Personaje NPersonaje)
	{		
		return posiciones.get(NPersonaje.getNombre());
	}
	
	public boolean jaqueMate(Personaje NPersonaje)
	{
		boolean jaqueMate = false;
		
		int i = 0;
		while(i < personajes.size()) {
			Personaje personaje = personajes.get(i);
			
			if(personaje != NPersonaje && personaje.getVidaActual() > 0) {
				int j = 0;
				while(j < personaje.getHabilidades().size()) {
					Habilidad habilidad = personaje.getHabilidades().get(j);
					
					if(distancia(posicion(personaje), posicion(NPersonaje)) <= habilidad.getRango() &&
							habilidad.getTipo() != TipoHabilidad.Sanar && 
							NPersonaje.getVidaActual() <= habilidad.getPoder()) { jaqueMate = true; }
					
					j++;
				}
			}
			
			i++;
		}
		
		return jaqueMate;
	}
	
	public ArrayList<Personaje> victimasPorCercania(Personaje NPersonaje)
	{
		ArrayList<Personaje> victimas = new ArrayList<Personaje>();
		
		int i = 0;
		while(i < personajes.size()) {
			Personaje personaje = personajes.get(i);
			
			if(personaje.getVidaActual() > 0) {
				int j = 0;
				while(j < NPersonaje.getHabilidades().size()) {
					Habilidad habilidad = NPersonaje.getHabilidades().get(j);
					
					if(distancia(posicion(NPersonaje), posicion(personaje)) <= habilidad.getRango())
					{ victimas.add(personaje); break; }
					
					j++;
				}
			}
			
			i++;
		}
		
		ArrayList<Integer> distancias = new ArrayList<Integer>();
		i = 0;
		while(i < victimas.size()) {
			Personaje victima = victimas.get(i);
			distancias.add(distancia(posicion(NPersonaje), posicion(victima)));
			
			i++;
		}
		
		i = 1;
		while(i < victimas.size()) {
			int j = i;
			
			while(j > 0 && distancias.get(j - 1) > distancias.get(j)) {
				Integer anterior = distancias.get(j - 1);
				distancias.set(j - 1, distancias.get(j));
				distancias.set(j, anterior);
				
				Personaje pAnterior = victimas.get(j - 1);
				victimas.set(j - 1, victimas.get(j));
				victimas.set(j, pAnterior);
				
				j--;
			}
			
			i++;
		}
		
		return victimas;
	}
	
	public ArrayList<Posicion> posicionesSeguras(Personaje NPersonaje)
	{
		ArrayList<Posicion> posiciones = new ArrayList<Posicion>();
		int i = 0;
		
		while(i < tablero.getAncho()) {
			int j = 0;
			
			while(j < tablero.getAlto()) {
				int k = 0;
				
				while(k < personajes.size()) {
					Personaje personaje = personajes.get(k);
					int h = 0;
					
					if(personaje.getVidaActual() > 0) {
						while(h < personaje.getHabilidades().size()) {
							Habilidad habilidad = personaje.getHabilidades().get(h);
							Posicion posicion = new Posicion(i, j);
							
							if(distancia(posicion(NPersonaje), posicion) > habilidad.getRango() &&
							   distancia(posicion(NPersonaje), posicion) <= NPersonaje.getVelocidad())
							{ posiciones.add(posicion); }
							
							h++;
						}
					}
					
					k++;
				}
				
				j++;
			}
			
			i++;
		}
		
		return posiciones;
	}
	
	public ArrayList<Personaje> losMasPoderosos()
	{
		int maximo = -1;
		int i = 0;
		while(i < personajes.size()) {
			int poder = personajes.get(i).getNivelPoder();
			if(poder > maximo) { maximo = poder; }
			
			i++;
		}
	
		i = 0;
		ArrayList<Personaje> poderosos = new ArrayList<Personaje>();
		while(i < personajes.size()) {
			Personaje personaje = personajes.get(i);
			
			int poder = personajes.get(i).getNivelPoder();
			if(poder == maximo) { poderosos.add(personaje); }
			
			i++;
		}
		
		return poderosos;
	}
	
	public Personaje elVengador()
	{
		Personaje vengador = null;
		int maximo = -1;
		int i = 0;
		while(i < personajes.size()) {
			Personaje nVengador = personajes.get(i);
			
			int vengados = 0;
			int j = 0;
			while(j < nVengador.getVictimas().size()) {
				Personaje victima = null;
				String nombreVictima = nVengador.getVictimas().get(j);
				
				int k = 0;
				while(k < personajes.size()) {
					if(personajes.get(k).getNombre() == nombreVictima) { victima = personajes.get(k); }
					k++;
				}
				
				vengados += victima.getVictimas().size();
				
				j++;
			}
			
			if(vengados > maximo) { maximo = vengados; vengador = nVengador; }
			
			i++;
		}
		
		return vengador;
	}
	
	public void teletransportacion()
	{
		if(personajes.size() > 0) {
			Personaje menorVida = null;
			Personaje mayorVida = null;
			
			int i = 0;
			while(i < personajes.size()) {
				Personaje personaje = personajes.get(i);
				
				if(personaje.getVidaActual() > 0) {
					if(menorVida == null || personaje.getVidaActual() < menorVida.getVidaActual())
					{ menorVida = personaje; }
					if(mayorVida == null || personaje.getVidaActual() > mayorVida.getVidaActual())
					{ mayorVida = personaje; }
				}
				
				i++;
			}

			System.out.println(menorVida);
			System.out.println(mayorVida);
			
			int vida = menorVida.getVidaActual();
			Posicion posicion = new Posicion(posicion(menorVida).getX(), posicion(menorVida).getY());
			
			if(mayorVida.getVidaActual() > menorVida.getVidaMaxima())
			{ menorVida.setVidaActual(menorVida.getVidaMaxima()); }
			else { menorVida.setVidaActual(mayorVida.getVidaActual()); }
			posicion(menorVida).setX(posicion(mayorVida).getX());
			posicion(menorVida).setY(posicion(mayorVida).getY());
			
			mayorVida.setVidaActual(vida);
			posicion(mayorVida).setX(posicion.getX());
			posicion(mayorVida).setY(posicion.getY());
		}
	}
	
	public void atacar(Personaje NPersonaje)
	{
		ArrayList<Personaje> victimas = victimasPorCercania(NPersonaje);
		
		Personaje victima = null;
		if(victimas.size() > 1) { victima = victimas.get(1); }
		else { victima = victimas.get(0); }
		
		Habilidad habilidad = NPersonaje.getHabilidades().get(0);
		if(distancia(posicion(NPersonaje), posicion(victima)) > habilidad.getRango()
				|| (habilidad.getTipo() == TipoHabilidad.Sanar &&
				NPersonaje.getVidaActual() == NPersonaje.getVidaMaxima()))
		{ habilidad = NPersonaje.getHabilidades().get(1); }
		
		if(habilidad.getTipo() != TipoHabilidad.Sanar) {
			if(victima.getVidaActual() - habilidad.getPoder() <= 0) {
				victima.setVidaActual(0);
				habilidad.setLevel(habilidad.getLevel() + 1);
				NPersonaje.agregarVictima(victima.getNombre());
				posiciones.remove(victima.getNombre());
			}
			else { victima.setVidaActual(victima.getVidaActual() - habilidad.getPoder()); }
		}
		else {
			victima = victimas.get(0);
			
			victima.setVidaActual(victima.getVidaActual() + habilidad.getPoder());
			if(victima.getVidaActual() > victima.getVidaMaxima())
			{ victima.setVidaActual(victima.getVidaMaxima()); }
		}
	}

	public int distancia(Posicion Posicion1, Posicion Posicion2)
	{
		return Math.abs(Posicion1.getX() - Posicion2.getX()) +
			   Math.abs(Posicion1.getY() - Posicion2.getY());
	}
	
    public static void Escribir(Writer Output, Juego NJuego) throws IOException
	{
    	Tablero.Escribir(Output, NJuego.getTablero());
    	
    	int i = 0;
    	ArrayList<Personaje> personajes = NJuego.getPersonajes();
    	Output.write("[ ");
    	while(i < personajes.size()) {
    		Personaje personaje = personajes.get(i);
	    	if(personaje.getVidaActual() > 0) {	
    			Output.write(personaje.getNombre() + " ");
	    		Posicion.Escribir(Output, NJuego.posicion(personaje));
		    	if(i < personajes.size() - 1) { Output.write(", "); }
	    	}
    		
    		i++;
    	}
    	Output.write("] ");
    	
    	i = 0;
    	Output.write("[ ");
    	while(i < personajes.size()) {
    		Personaje.Escribir(Output, personajes.get(i));
    		
		    if(i < personajes.size() - 1) { Output.write(", "); }
    		
    		i++;
    	}
    	Output.write("] ");
	}
	
	public static Juego Leer(StringTokenizer Input) throws IOException
	{
		Tablero tablero = Tablero.Leer(Input);
		
		Map<String, Posicion> posiciones = new HashMap<String, Posicion>();
		String token = Input.nextToken();
		if(token.equals("[")) {
			token = Input.nextToken();
			
			while(!token.equals("]")) {
				if(token.equals(",")) { token = Input.nextToken(); }
				String nombre = token;
				
				Posicion posicion = Posicion.Leer(Input);
				
				posiciones.put(nombre, posicion);
				
				token = Input.nextToken();
			}
		}
		
		List<Personaje> personajes = new ArrayList<Personaje>();
		token = Input.nextToken();
		if(token.equals("[")) {
			Personaje personaje = Personaje.Leer(Input);
			
			while(personaje != null) {
				personajes.add(personaje);
				
				personaje = Personaje.Leer(Input);
			}
		}
		
		Juego juego = new Juego(personajes, posiciones, tablero);
		
		return juego;
    }
	
	private static String posicionesString(Juego juego) {
		ArrayList<Personaje> personajes = juego.getPersonajes();
		
		String posiciones = "";
		
		int i = 0;
		posiciones += "[";
		while(i < personajes.size()) {
			if(personajes.get(i).getVidaActual() > 0) {
				posiciones += personajes.get(i).getNombre() + " " + juego.posicion(personajes.get(i));
			    if(i < personajes.size() - 1) { posiciones += ", "; }
			}
			
			i++;
		}
		posiciones += "]";
		
		return posiciones;
	}
	
}