package tpalgo1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.StringTokenizer;

public class Main {

	private static Juego juego;
	
	public static void main(String[] args) throws IOException {
		String fileDirLeer = "src/Juego2.txt";
		String fileDirEscribir = "src/SJuego2.txt";
		
		BufferedReader bufferReader = new BufferedReader(new FileReader(new File(fileDirLeer)));
		StringTokenizer stringTokenizer = new StringTokenizer(bufferReader.readLine());
		juego = Juego.Leer(stringTokenizer);
		bufferReader.close();
		
		System.out.println(juego);
		System.out.println();

		if(juego.getPersonajes().size() > 0) {
			int i = 0;
			while(i < juego.getPersonajes().size()) {
				Personaje personaje = juego.getPersonajes().get(i);
	
				System.out.println("" + personaje);
				if(personaje.getVidaActual() > 0) {
					System.out.println("Esta en jaque: " + juego.jaqueMate(personaje));
					System.out.println("Victimas por cercania: " + juego.victimasPorCercania(personaje));
				} else { System.out.println("R.I.P"); }
				System.out.println();
					
				i++;
			}
	
			System.out.println("El mas poderoso: " + juego.losMasPoderosos());
			System.out.println("El vengador: " + juego.elVengador());
			juego.atacar(juego.getPersonajes().get(0));
			juego.teletransportacion();
			
			System.out.println(juego);
			System.out.println();
		}
		
		Writer writer = new FileWriter(new File(fileDirEscribir));
		Juego.Escribir(writer, juego);
		writer.close();
	}

}
