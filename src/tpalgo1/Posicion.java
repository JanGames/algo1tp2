package tpalgo1;

import java.io.Writer;
import java.util.StringTokenizer;
import java.io.IOException;

public class Posicion 
{	
	private int x;
	private int y;

	@Override
	public String toString() { return "(X: " + x + ", Y: " + y + ")"; };
	
	public Posicion(int X, int Y)
	{
		setX(X);
		setY(Y);
	}
	
	public int getX() {	return x; }
	public void setX(int X) { x = X; }

	public int getY() {	return y; }
	public void setY(int Y) { y = Y; }
    
	public static void Escribir(Writer Output, Posicion NPosicion) throws IOException
	{
		Output.write("( " + NPosicion.getX() + " , " + NPosicion.getY() + " ) ");
	}
	
	public static Posicion Leer(StringTokenizer Input) throws IOException
	{
		int[] dupla = LeerUtil.leerDupla(Input);
		Posicion posicion = new Posicion(dupla[0], dupla[1]);
		
		return posicion;
    }
	
}