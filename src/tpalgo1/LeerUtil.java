package tpalgo1;

import java.io.IOException;
import java.util.StringTokenizer;

public class LeerUtil {
	
	public static int[] leerDupla(StringTokenizer Input) throws IOException
	{
		int[] dupla = new int[2];
		
		if(Input.nextToken().equals("(")) {
			dupla[0] = Integer.valueOf(Input.nextToken());
			
			if(Input.nextToken().equals(",")) {	dupla[1] = Integer.valueOf(Input.nextToken()); }
			else { System.err.println("La dupla se separa con coma"); }
			
			if(!Input.nextToken().equals(")")) { System.err.println("La dupla debe terminar con )"); }
		}
		else { System.err.println("La dupla debe empezar con ("); }
		
		return dupla;
	}
	
}
